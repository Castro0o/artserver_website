=Renée Turner's Warp Weft Memory=

The Warp and Weft of Memory is a research project with [http://castrumperegrini.org/ Castrum Peregrin]i, which ran from September 2016 to October 2018.  The work explores the wardrobe of [https://nl.wikipedia.org/wiki/Gis%C3%A8le_d'Ailly-van_Waterschoot_van_der_Gracht Gisèle d’Ailly van Waterschoot van der Gracht] and the ways in which it reflects her life, work, and various histories through textiles and clothing. The research not only mined the past but also made connections to the present. As a whole, the project had different public manifestations: public lectures, educational events, an online narrative combining fact, fiction and artefacts.


{{Thumb|File:The Warp and Weft of Memory.jpg}}
{{Thumb|File:The Warp and Weft of Memory-1.jpg}}
{{Thumb|File:The Warp and Weft of Memory-3.jpg}}


* '''Author:''' [http://www.fudgethefacts.com/ Renée Turner]
* '''MediaWiki Design:''' André Castro and [http://randomiser.info/ Cristina Cochior]
* '''MediaWiki Infrastructure & Server Maintenance:''' André Castro
* '''Interface Design and Development''': [http://manufacturaindependente.org/ Manufactura Independente]
* '''Illustrations''': [http://www.visualiser.org/ Cesare Davolio]
* '''URL''': http://warpweftmemory.net/

===Media:===
* [https://www.pandemic.space/2018/10/31/the-warp-and-weft-of-memory/ Pandemic - Renee Turner: The Warp and Weft of Memory]
* [https://rasl.nu/research-interview-renee-turner/ RAS - Research Interview: Renée Turner]


