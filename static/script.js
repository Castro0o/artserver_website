var colors = ['rgb(249, 17, 0)', 'rgb(255, 131, 10)', 'rgb(191, 234, 21)', 'rgb(21, 234, 123)', 'rgb(7, 221, 201)', 'rgb(10, 186, 255)', 'rgb(26, 37, 210)', 'rgb(238, 23, 181)']
var randomcolor = colors[ Math.floor(Math.random()*((colors.length-1))) ] ;

function addCss(cssCode) {
var styleElement = document.createElement("style");
styleElement.type = "text/css";
if (styleElement.styleSheet) {
styleElement.styleSheet.cssText = cssCode;
} else {
styleElement.appendChild(document.createTextNode(cssCode));
}
document.getElementsByTagName("head")[0].appendChild(styleElement);
}

function coloring(color){
var randommenuborder = '0.08em solid ' + color;
var randomlinkborder = '0.08em solid' + color;
var blacklinkborder = '0.08em solid #000';
var randomlinkbordermenu = '0.08em solid' + color; 
var blacklinkbordermenu  = '0.08em solid #000'; 
var selection = '::selection {color:black;background:'+ color + ';}';
var selection_moz = '::-moz-selection {color:black;background:'+ color + ';}';

$('li.header-separator').css('color', randomcolor);
$('div.header#top-header').css('border-bottom', randommenuborder);
$('span.random').css('color', randomcolor);
$('div#space').css('background-color', randomcolor); 
$('p.gototop').css('color', randomcolor); 
$('div#clients-menu').css('border-top', 'solid 1px ' + randomcolor);
$('div#contact-menu').css('border-top', 'solid 1px ' + randomcolor);
$('div#about-menu').css('border-top', 'solid 1px ' + randomcolor);
// $('a').css('border-bottom', '1px solid ' + randomcolor);


addCss(selection);
addCss(selection_moz);

$('a.body').hover(
function(){
$(this).css('border-bottom', randomlinkborder);
},
function(){
$(this).css('border-bottom', blacklinkborder);
})

$('a.en,a.pt').hover(
function(){
$(this).css('border-bottom', randomlinkborder);
},
function(){
$(this).css('border-bottom', blacklinkborder);
})

$('a.clients, a.header-menu').hover(
function(){
$(this).css('border-bottom', randomlinkborder);
},
function(){
$(this).css('border-bottom', 'none');
})
}

function autoscroll(pos){
$('html,body').animate({
scrollTop: pos - 65
}, 1500);
return false;
}


$(document).ready(
	function() {
		coloring(randomcolor);
		window.scrollTo(0,0);

		var w_height = $(window).height();
		$('a.header-menu').click(
		function(){
			var pos_h = $('div.content-text#'+$(this).attr('id')+'-menu');
			var thisid = $(this).attr('id');
			autoscroll(pos_h.position().top);
		})

		$('p.gototop').click(
		function(){
		autoscroll(0);
		})
})
