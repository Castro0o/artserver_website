#!/usr/bin/env python3

import subprocess
import os
import re
import argparse
import yaml
from collections import OrderedDict
from pprint import pprint

from jinja2 import FileSystemLoader
from jinja2.environment import Environment


parser = argparse.ArgumentParser()
parser.add_argument('--noimages', action='store_true')
args = parser.parse_args()
if args.noimages is False:
    from wiki_imgs import *

yaml_metadata = ''

regex_img = re.compile(r'\[\[File\:(?P<fn>.*\.\w{3,4})\]\]')
regex_thumb = re.compile(r'\{\{Thumb\|File\:(?P<fn>.*\.\w{3,4})\}\}')
regex_widget_yt = re.compile(r'\{\{\#widget\:Youtube\|id\=(.*)\}\}')


def pandoc(inputfile, format_in, format_out):
    pandoc_cmd = "pandoc {} -f {} -t {}".format(
        inputfile, format_in, format_out)
    output = subprocess.check_output(pandoc_cmd, shell=True)
    output = output.decode('utf-8')
    return output


def capture_images(wiki_content):
    '''
    returns
    * wiki_file_content: content without image tags
    * images_in_page: {'imagename':{'url':'...', 'caption': '...'}}
    '''
    imgs_in_page = {}
    wiki_content_noimgs = wiki_content
    for regex in [regex_img, regex_thumb]:
        imgs_found = re.findall(regex, wiki_content)
        imgs_in_page.update(dict((img, None) for img in imgs_found))
        wiki_content_noimgs = re.sub(regex, '', wiki_content_noimgs)
    for imgname in imgs_in_page.keys():
        img_url, img_caption = img_info(imgname)
        imgs_in_page[imgname] = {'url': img_url}
        if img_caption not in ['', None]:
            imgs_in_page[imgname].update({'caption': img_caption})
    return wiki_content_noimgs, imgs_in_page

# TODO: when displaying file, in template, 
# check if it is not video (.mp4,ogv, webm )
# or audio (mp3, ogv). if so use html player


def capture_widget(wiki_content):
    # for the moment is hardcoded to the Youtube widget,
    # but could accommodate other
    widgets_found = re.findall(regex_widget_yt, wiki_content)
    wiki_content_nowidget = re.sub(regex_widget_yt, '', wiki_content)
    # returns list of youtube ids
    return wiki_content_nowidget, widgets_found


def jinja_env(templates_dir):
    env = Environment()
    env.loader = FileSystemLoader(templates_dir)
    return env


def jinja_populate_template(env, template, title, sections,
                            pages_list, outfile, metatags=None):
    tmpl = env.get_template(template)
    if template == 'metatags.html':
        tmpl_rendered = tmpl.render(author=sections.get('author'),
                                    url=sections.get('url'),
                                    description=sections.get('description'),
                                    title=sections.get('title'),
                                    shortdescription=sections.get('title'),
                                    imgurl=sections.get('imgurl'))
        return tmpl_rendered
    else:
        tmpl_rendered = tmpl.render(title=title,
                                    sections=sections,
                                    pages_list=pages_list,
                                    metatags=metatags)

        with open(outfile, 'w') as index:
            index.write(tmpl_rendered)


def order_files(txtfiles):
    # order txtfiles, with works in reverse over (latest first)
    txtfiles.sort()
    works = [txtfile for txtfile in txtfiles if txtfile.startswith('02')]
    txtfiles_slice0 = txtfiles[0: txtfiles.index(works[0])]
    txtfiles_slice1 = txtfiles[txtfiles.index(works[-1]) + 1:]
    works = works[::-1]  # invert the order of works
    txtfiles = txtfiles_slice0 + works + txtfiles_slice1
    txtfiles = [os.path.join(txt_files_dir, txtfile)
                for txtfile in txtfiles]
    return txtfiles


class Page:
    def __init__(self, input_file, in_format, out_format):
        self.input_file = input_file
        self.in_format = in_format
        self.out_format = out_format
        self.section = os.path.basename(input_file)[:-len('.wiki')].lower()
        self.name = self.section

    def read_input(self):
        with open(self.input_file, 'r') as wiki_file_r:
            self.input_content = wiki_file_r.read()

    def process_imgs(self):
        self.input_content, self.imgs_in_page = capture_images(
            self.input_content)
    #     # self.imgs_in_page is dict
    #     {imgname:
    #         {'url': '...',
    #         'caption':'...'},
    #         }

    def process_widgets(self):
        self.input_content, self.youtube = capture_widget(self.input_content)

    def convert(self):
        tmp_file = os.path.join(os.path.abspath('.'), 'tmp.wiki')
        with open(tmp_file, 'w') as tmp_file_w:
            tmp_file_w.write(self.input_content)
        content_html = pandoc(tmp_file, self.in_format, self.out_format)
        self.output_content = str(content_html)

    def work_year(self):
        self.year = self.name.split('_')[2]


if __name__ == '__main__':
    pages_list = []
    txt_files_extension = '.wiki'
    txt_files_dir = os.path.join(os.path.abspath('.'), 'wiki_files')
    txtfiles = [txtfile for txtfile in os.listdir(txt_files_dir)
                if txtfile.endswith(txt_files_extension)]

    metatada_yaml = os.path.join(txt_files_dir, 'metadata.yaml')
    with open(metatada_yaml, 'r') as metadata_f:
        metadata = yaml.load(metadata_f)

    txtfiles = order_files(txtfiles)
    for txt_file in txtfiles:
        page = Page(txt_file, 'mediawiki', 'html')
        page.read_input()
        if args.noimages is False:
            print('processing imgs')
            page.process_imgs()
        page.process_widgets()
        page.convert()
        if '_work_' in txt_file:
            page.work_year()
        pages_list.append(page)

    print('pages_list:', pages_list)
    menu_sections = [page.section.split('_')[1] for page in pages_list]
    menu_sections = list(OrderedDict.fromkeys(menu_sections))

    # TODO put files in yaml file

    env = jinja_env('./templates')
    metatags = jinja_populate_template(env,
                                       template='metatags.html',
                                       title='André Castro @ Artserver',
                                       sections=metadata,
                                       pages_list=None,
                                       outfile='website/metatags.html')
    jinja_populate_template(env,
                            template='main.html',
                            title='André Castro',
                            sections=menu_sections,
                            pages_list=pages_list,
                            metatags=metatags,
                            outfile='website/index.html')


